function vidplay() {
  var video = document.getElementById("Video1");
  var button = document.getElementById("play");
  if (video.paused) {
    video.play();
    button.textContent = "||";
  } else {
    video.pause();
    button.textContent = ">";
  }
};

$(document).ready(function() {
  $("#Video1").on("timeupdate",  function(event) {
    onTrackedVideoFrame(this.currentTime);
  });
});

function onTrackedVideoFrame(currentTime) {
  dataGone = (currentTime * 0.0727) //data videa za vteřinu
  $("#data").html('Spotřebováno <b>' + Math.round(dataGone * 100) / 100 + ' MB</b> dat<br>' 
    + 'Češi v průměru zaplatí <b>' + Math.round((dataGone * 0.204) * 100) / 100 + ' Kč</b>, '
    + 'Slováci v přepočtu <span class="less">' + Math.round((dataGone * 0.182) * 100) / 100 + ' Kč</span>, '
    + 'Němci <span class="more">' + Math.round((dataGone * 0.332) * 100) / 100 + ' Kč</span><br>'
    + 'Poláci <span class="less">' + Math.round((dataGone * 0.024) * 100) / 100 + ' Kč</span> a '
    + 'Rakušané <span class="less">' + Math.round((dataGone * 0.073) * 100) / 100 + ' Kč</span>'
  );
};